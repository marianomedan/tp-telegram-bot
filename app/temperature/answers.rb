module Temperature
  class Answers
    def initialize; end

    def self.is_it_hot
      latitude = '-34.603722'
      longitude = '-58.381592'
      openweathermap_api_key = '6fd1a4f5868dfa4c96953146f15c8435'
      response = Faraday.get("http://api.openweathermap.org/data/2.5/weather?lat=#{latitude}&lon=#{longitude}&appid=#{openweathermap_api_key}&units=metric")
      data = JSON.parse(response.body)
      temperature = data['main']['temp'].to_i

      if temperature > 30
        'hace calor, ponete los cortos'
      elsif temperature < 10
        'ta fresco, ponete los largos'
      else
        'ta bien, dale tranqui'
      end
    end
  end
end
